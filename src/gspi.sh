#!/bin/sh
# gspi - Graphical Slackware Package Installer
# A try to make gdebi-like interface to Slackware

export NAME=gspi
export VER=0.2
export TITLE="$NAME-yad"
export SLACKWARE_VERSION=$(cat /etc/slackware-version | sed 's/Slackware //')
case $SLACKWARE_VERSION in
	15.0)
		export PKGSTATE_DIR=/var/lib/pkgtools/packages
		;;
	*curren*)
		export PKGSTATE_DIR=/var/lib/pkgtools/packages
		;;
	*)
		export PKGSTATE_DIR=/var/log/packages
		;;
esac

var_set()
{
	VARCONTENT=$(echo $* | sed "s/$1//")
	VARNAME=$1
	rm -rf /tmp/org_glowiak_gspi_varset_var_$VARNAME
	echo "$VARCONTENT" > /tmp/org_glowiak_gspi_varset_var_$VARNAME
	log Set variable $VARNAME to $VARCONTENT
}
var_get()
{
	VARNAME=$1
	if [ ! -f /tmp/org_glowiak_gspi_varset_var_$VARNAME ]
	then
		echo "WARNING: variable does not exist!"
	fi
	cat /tmp/org_glowiak_gspi_varset_var_$VARNAME
}
log()
{
	echo "INFO: $*"
}
ginfo()
{
	log Running yad
	yad --title "$TITLE" --text "$*"
}
warning()
{
	echo "WARNING: $*"
}
error()
{
	echo "ERROR: $*"
	exit 1
}
gerror()
{
	log Running yad
	yad --title "ERROR" --text "$*"
	exit 1
}

log Starting $NAME version $VER with yad on Slackware $SLACKWARE_VERSION

if [ ! -f /usr/bin/yad ]
then
	error Please install yad!
fi

if [ -f "$1" ]
then
	export TASK=install
fi
if [ ! -f "$1" ]
then
	export TASK=menu
fi
if [ "$1" == "--about" ]
then
	export TASK=about
fi
if [ "$1" == "--help" ]
then
	export TASK=help
fi
if [ "$1" == "--store" ]
then
	export TASK=software-center
fi
if [ "$1" == "--selectpkg" ]
then
	export TASK=selectpkg
fi
log Starting task $TASK


case $TASK in
	about)
		log Running yad
		yad --title "$TITLE" --text "$NAME version ${VER}\nGraphical Slackware Package Installer\nUsing YAD version $(yad --version)"
		bash $0
		rm -rf /tmp/org_glowiak_gspi_varset_var*
		rm -rf /tmp/com_proginfo_*
		exit 0
		;;
	help)
		log Running yad
		yad --title "$TITLE" --text "no help to show at all (seriously!)."
		bash $0
		rm -rf /tmp/org_glowiak_gspi_varset_var*
		rm -rf /tmp/com_proginfo_*
		;;
	software-center)
		# check for root privilages
		if [ "$(whoami)" != "root" ]
		then
			log Running with root privilages
			sudo bash $0 --store
			exit 0
		fi
		log Viewing categories menu
		# check for sbosnap runt
		if [ ! -d /usr/sbo ]
		then
			# gerror "Please run 'sbosnap fetch' first!"
			log Running yad
			yad --title "$TITLE" --width=310 --height=400 --text "Looks like you haven't bootstraped SBo yet." --button "Close $NAME" --button "Bootstrap SBo"
			XTASK1=$?
			log Output: $XTASK1
			if [ "$XTASK1" == "0" ]
			then
				rm -rf /tmp/org_glowiak_gspi_varset_var*
				rm -rf /tmp/com_proginfo_*
				exit 0
			fi
			log Bootstrapping SBo
			xterm -title "SBo Bootstrap" -e "sbosnap fetch"
			ginfo "Suckcessfuly bootstrapped SBo"
		fi
		# display categories list
		NUMBER=0
		for all in $(ls /usr/sbo/repo/)
		do
			if [ -d /usr/sbo/repo/$all ]
			then
				NUMBER=$(expr $NUMBER + 1)
				log Found category ${NUMBER}. $all
				CATEGORIES="$CATEGORIES $all"
				var_set CATEGORY_${NUMBER} $all
			fi
		done
		log Categories found: $CATEGORIES
		log Running yad
		SECAT1=$(yad --list --width=310 --height=400 --title "$TITLE" --text "Select category" --column="Category" $CATEGORIES)
		SECAT=$(echo $SECAT1 | sed 's/|//g')
		log "Selected category: $SECAT"
		# display programs list
		NUMBER2=1
		for prog in $(ls /usr/sbo/repo/$SECAT)
		do
			if [ -d /usr/sbo/repo/$SECAT/$prog ]
			then
				NUMBER2=$(expr $NUMBER2 + 1)
				log Found program ${NUMBER2}. $prog
				PROGRAMS="$PROGRAMS $prog"
				var_set PROGRAM_${NUMBER2} $prog
			fi
		done
		SEPROG1=$(yad --list --width=310 --height=400 --title "$TITLE" --text "Select program" --column="Program" $PROGRAMS)
		SEPROG=$(echo $SEPROG1 | sed 's/|//g')
		log Selected program: $SEPROG
		# display program information
		log Downloading package metadata
		wget -O /tmp/com_proginfo_$SEPROG http://slackbuilds.org/slackbuilds/$SLACKWARE_VERSION/$SECAT/$SEPROG/$SEPROG.info
		. /tmp/com_proginfo_$SEPROG
		log Running yad
		if [ ! "$(ls $PKGSTATE_DIR/$SEPROG-*.*)" == "" ]
		then
			AINST="yes"
			yad --title "$TITLE" --width=310 --height=400 --text "Name: ${SEPROG}\nVersion: ${VERSION}\nHomepage: ${HOMEPAGE}\nDependiences: ${REQUIRES}\nMaintainer: ${MAINTAINER}\nAlready installed: ${AINST}" --button "Exit" --button "Uninstall"
			YTASK2=$?
		fi
		if [ "$(ls $PKGSTATE_DIR/$SEPROG-*.*)" == "" ]
		then
			AINST="no"
			yad --title "$TITLE" --width=310 --height=400 --text "Name: ${SEPROG}\nVersion: ${VERSION}\nHomepage: ${HOMEPAGE}\nDependiences: ${REQUIRES}\nMaintainer: ${MAINTAINER}\nAlready installed: ${AINST}" --button "Exit" --button "Install"
			YTASK2=$?
		fi
		# YTASK2=$?
		log Output: $YTASK2
		case $YTASK2 in
			0)
				rm -rf /tmp/org_glowiak_gspi_varset_var*
				rm -rf /tmp/com_proginfo_*
				bash $0
				rm -rf /tmp/org_glowiak_gspi_varset_var*
				rm -rf /tmp/com_proginfo_*
				;;
			1)
				case $AINST in
					no)
						log Starting installation of $SEPROG
						xterm -title "Installing package $SEPROG" -e "yes | sboinstall $SEPROG"
						ginfo Installed package $SEPROG
						;;
					yes)
						log Starting deinstallation of $SEPROG
						xterm -title "Removing package $SEPROG" -e "removepkg $SEPROG"
						ginfo Removed package $SEPROG
						;;
				esac
				;;
		esac
		bash $0
		rm -rf /tmp/org_glowiak_gspi_varset_var*
		rm -rf /tmp/com_proginfo_*
		;;
	menu)
		log Running yad
		yad --title "$TITLE" --text "Welcome to $NAME!\nWhat do you want to do?" --button "Install a local package" --button "Install a package from SBo" --button "About $NAME" --button "Exit"
		OUTPUT_MENU=$?
		log Output: $OUTPUT_MENU
		case $OUTPUT_MENU in
			0)
				bash $0 --selectpkg
				rm -rf /tmp/org_glowiak_gspi_varset_var*
				rm -rf /tmp/com_proginfo_*
				exit 0
				;;
			1)
				bash $0 --store
				rm -rf /tmp/org_glowiak_gspi_varset_var*
				rm -rf /tmp/com_proginfo_*
				exit 0
				;;
			2)
				bash $0 --about
				rm -rf /tmp/org_glowiak_gspi_varset_var*
				rm -rf /tmp/com_proginfo_*
				exit 0
				;;
			3)
				rm -rf /tmp/org_glowiak_gspi_varset_var*
				rm -rf /tmp/com_proginfo_*
				exit 0
				;;
		esac
		;;
	selectpkg)
		log Running yad
		FILE_PACKAGE="$(yad --file-selection --title Selection --width=310 --height=400)"
		bash $0 "$FILE_PACKAGE"
		bash $0
		rm -rf /tmp/org_glowiak_gspi_varset_var*
		rm -rf /tmp/com_proginfo_*
		exit 0
		;;
	install)
		FPKG="$1"
		if [ ! "$(whoami)" == "root" ]
		then
			log Restarting with root privilages
			sudo bash $0 $FPKG
			rm -rf /tmp/org_glowiak_gspi_varset_var*
			rm -rf /tmp/com_proginfo_*
			exit 0
		fi
		log Running yad
		FPKG1=$(basename $FPKG)
		SEPROG2=$(echo $FPKG1 | sed "s/.tgz//")
		if [ "$(ls $PKGSTATE_DIR/$SEPROG2)" == "" ]
		then
			AINST2="no"
			yad --title "$TITLE" --width=310 --height=400 --text "Package installation\nFile name: ${FPKG}\nPackage name: ${SEPROG2}\nAlready installed: ${AINST2}" --button "Exit" --button "Install"
			YTASK3=$?
		fi
		if [ ! "$(ls $PKGSTATE_DIR/$SEPROG2)" == "" ]
		then
			AINST2="yes"
			yad --title "$TITLE" --width=310 --height=400 --text "Package installation\nFile name: ${FPKG}\nPackage name: ${SEPROG2}\nAlready installed: ${AINST2}" --button "Exit" --button "Uninstall"
			YTASK3=$?
		fi
		# YTASK3=$?
		log YTASK3: $YTASK3
		if [ "$YTASK3" == "0" ]
		then
			rm -rf /tmp/org_glowiak_gspi_varset_var*
			rm -rf /tmp/com_proginfo_*
			exit 0

		fi
		case $AINST2 in
			no)
				log Beginning installation
				xterm -title "Package installation" -e "/sbin/installpkg $FPKG"
				ginfo "Installed package $SEPROG2"
				;;
			yes)
				log Beginning uninstallation
				xterm -title "Package deinstallation" -e "/sbin/removepkg $FPKG"
				ginfo "Removed package $SEPROG2"
				;;
		esac
esac
# clean hundreds of var files in /tmp
rm -rf /tmp/org_glowiak_gspi_varset_var*
rm -rf /tmp/com_proginfo_*
exit 0

# gspi

Graphical Slackware Package Installer is a tool for Slackware Linux that conbines features of both GDebi and Synaptic. But it's mostly a graphical frontend for sbotools and Slackware's internal pkg tools.

### Dependiences

To use gspi you will need:

-sbotools to use the software center

-yad for the gui

-sudo with system paths (/sbin) and no asking for password if you want to run it as normal user

### Installation

Use [this SlackBuild](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/gspi.md)

or if you feel superior download latest release tarball, and type make install as root

### Features

[DONE] Gdebi-like package installation

[DONE] synaptic-like software center

[DONE] Slackware 15.0 support

[DONE] automatically choosing slackware version that is used

[WIP] support for package extension other than tgz

### How to use it

You can set "gspi" program to open .tgz packages, it will open a gdebi-like gui, where you can install (or uninstall if it's already installed) a local package.

You can also use "gspi Software Center" app to browse available categories and applications. All apps come from SlackBuilds.org

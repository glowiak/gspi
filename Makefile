PREFIX=	/usr
DESTDIR=	

all:
	@echo targets: install

install:
	mkdir -pv ${DESTDIR}${PREFIX}/bin ${DESTDIR}${PREFIX}/share/applications
	cp -v ./src/gspi.sh ${DESTDIR}${PREFIX}/bin/gspi
	chmod 775 ${DESTDIR}${PREFIX}/bin/gspi
	cp -v ./src/gspi.desktop ${DESTDIR}${PREFIX}/share/applications/gspi.desktop
	chmod 775 ${DESTDIR}${PREFIX}/share/applications/gspi.desktop
	cp -v ./src/gspi-software.desktop ${DESTDIR}${PREFIX}/share/applications/gspi-software.desktop
	chmod 775 ${DESTDIR}${PREFIX}/share/applications/gspi-software.desktop
